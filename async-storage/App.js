import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react";
import {
  Button,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";

const App = () => {
  const [text, setText] = useState("");

  const saveName = async () => {
    try {
      AsyncStorage.setItem("appData", text);
    } catch (error) {
      console.log(error);
    }
  };

  const getName = async () => {
    try {
      const name = await AsyncStorage.getItem("appData");
      setText(name);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteName = () => {
    AsyncStorage.removeItem("appData");
    setText("");
  };

  useEffect(() => {
    getName();
  }, []);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View>
          <TextInput
            style={styles.textInput}
            placeholder="Input name"
            onChangeText={(value) => setText(value)}
          />
        </View>
        <View>
          <Text style={styles.nameText}>Name: {text}</Text>
          <Button title="Save name" onPress={saveName} />
          <Button color={"red"} title="Delete name" onPress={deleteName} />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  nameText: {
    textAlign: "center",
    fontSize: 20,
    marginVertical: 20,
    fontWeight: "bold",
    color: "black",
  },
  container: {
    paddingTop: 100,
    paddingHorizontal: 20,
  },
  textInput: {
    height: 50,
    borderColor: "blue",
    borderWidth: 1,
    justifyContent: "center",
    paddingHorizontal: 20,
  },
});

export default App;
