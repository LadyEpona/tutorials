All course videos for the CSS Grid Layouts tutorial series on the Net Ninja YouTube channel.

Build Layouts with CSS Grid #1 - CSS Grid Basics
https://www.youtube.com/watch?v=xPuYbmmPdEM

Build Layouts with CSS Grid #2 - Multi-Column Layout
https://www.youtube.com/watch?v=5cIMvD2ZDo0

Build Layouts with CSS Grid #3 - Holy Grail Layout
https://youtu.be/cJvMbQq0MIQ

Build Layouts with CSS Grid #4 - Masonry Style Layout (part 1) & (part 2)
https://youtu.be/OF7jjbgw0H0
https://youtu.be/J8H122wQAKU
