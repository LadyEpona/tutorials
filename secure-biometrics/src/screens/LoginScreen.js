import React, { useEffect, useState } from "react";
import {
  Alert,
  StyleSheet,
  Switch,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import * as LocalAuthentication from "expo-local-authentication";
import * as SecureStore from "expo-secure-store";
import { LinearGradient } from "expo-linear-gradient";

const save = async (key, value) => {
  await SecureStore.setItemAsync(key, value);
};

const getValueFor = async (key) => {
  let result = await SecureStore.getItemAsync(key);
  if (result) {
    alert("Here's your value: \n \n" + result);
  } else {
    alert("No values stored under that key");
  }
};

const LoginScreen = ({ navigation }) => {
  const [key, onChangeKey] = useState("");
  const [loginKey, onChangeLoginKey] = useState("");
  const [value, onChangeValue] = useState("");
  const [biometrics, setBiometrics] = useState(false);
  const [enrolled, setEnrolled] = useState(false);

  useEffect(() => {
    (async () => {
      const compatible = await LocalAuthentication.hasHardwareAsync();
      setBiometrics(compatible);
      const isEnrolled = await LocalAuthentication.isEnrolledAsync();
      setEnrolled(isEnrolled);
    })();
  });

  const onBioAuth = () => {
    const bioAuth = LocalAuthentication.authenticateAsync({
      promptMessage: "Tunnistaudu",
      fallbackLabel: "Enter password",
    });
    bioAuth.then((result) => {
      console.log(result);
      if (loginKey.match(key)) {
        navigation.navigate("HomeScreen");
      } else {
        console.log("Loginkey isn't same as key");
      }
    });
  };

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#00AAFF", "#00FF6C"]}
        style={styles.background}
      />
      <Text style={styles.title}>Testing Biometrics</Text>
      <Text style={styles.title2}>With SecureStore</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.textInput}
          placeholder="Username"
          onChangeText={(key) => onChangeKey(key)}
          value={key}
        />
        <TextInput
          style={styles.textInput}
          placeholder="Password"
          onChangeText={(value) => onChangeValue(value)}
          value={value}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            save(key, value);
            onChangeKey("");
            onChangeValue("");
          }}
        >
          <Text style={{ fontSize: 18 }}>Store</Text>
        </TouchableOpacity>
        <Text style={{ marginTop: 50, fontSize: 18 }}>Enter your key</Text>
        <TextInput
          style={styles.textInput}
          onChangeText={(loginKey) => onChangeLoginKey(loginKey)}
          placeholder="Enter your username"
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            if (loginKey !== null || undefined) {
              onBioAuth();
            } else {
              console.log("Error on bio logging");
            }
          }}
        >
          <Text style={{ fontSize: 18 }}>Bio Auth</Text>
        </TouchableOpacity>
        <Text style={{ marginTop: 25, fontSize: 18 }}>
          {biometrics
            ? "Your device is compatible with Biometrics"
            : "Your device is not compatible with Biometrics"}
        </Text>
        <Text style={{ marginTop: 15, fontSize: 18 }}>
          {enrolled
            ? "You have set up data for Biometrics"
            : "You have not set up data for Biometrics, please go to your settings"}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#00FF6C",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  background: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    height: 300,
  },
  button: {
    width: "65%",
    height: 40,
    backgroundColor: "#00AAFF",
    justifyContent: "center",
    alignItems: "center",
    margin: 6,
    borderRadius: 5,
  },
  inputContainer: {
    width: "100%",
    position: "absolute",
    top: 220,
    alignItems: "center",
  },
  title: {
    fontSize: 35,
    position: "absolute",
    top: 100,
    fontWeight: "bold",
  },
  title2: {
    fontSize: 35,
    position: "absolute",
    top: 150,
    fontWeight: "bold",
  },
  textInput: {
    height: 50,
    width: "65%",
    justifyContent: "center",
    paddingHorizontal: 20,
    backgroundColor: "#ffffffbf",
    textAlign: "center",
    borderRadius: 5,
    fontSize: 18,
    margin: 6,
  },
});

export default LoginScreen;
