import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

const HomeScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#00AAFF", "#00FF6C"]}
        style={styles.background}
      />
      <Text style={styles.welcomeText}>
        Welcome back! {/* {userDetails.username} */}
      </Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          navigation.navigate("LoginScreen");
        }}
      >
        <Text style={{ fontSize: 18 }}>Logout</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  background: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    height: 300,
  },
  button: {
    width: "65%",
    height: 40,
    backgroundColor: "#00AAFF",
    justifyContent: "center",
    alignItems: "center",
    margin: 6,
    borderRadius: 5,
  },
  container: {
    flex: 1,
    backgroundColor: "#00FF6C",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  welcomeText: {
    fontSize: 25,
  },
});

export default HomeScreen;
