import {
  StyleSheet,
  Text,
  TouchableHighlight,
  Button,
  Image,
  Alert,
  SafeAreaView,
  Platform,
  StatusBar,
} from "react-native";

export default function App() {
  const handlePress = () => console.log("Text pressed with handle");

  // SafeAreaView is only for iOS 11 -> adds some paddingTop to dodge the black phone structure
  // local images don't need size properties because require reads the metadata
  // fadeDuration only for Android
  // Alert.prompt is only for iOS
  return (
    <SafeAreaView style={styles.container}>
      <Text numberOfLines={1} onPress={handlePress}>
        Hello World! This is a test for making long text but keeping it in just
        one line for whatever purposes.
      </Text>
      <Image source={require("./app/assets/favicon.png")} />
      <TouchableHighlight onPress={() => console.log("Image tapped")}>
        <Image
          blurRadius={10}
          fadeDuration={1000}
          source={{
            width: 200,
            height: 300,
            uri: "https://picsum.photos/200/300",
          }}
        />
      </TouchableHighlight>
      <Button
        color="orange"
        title="Click me"
        onPress={() =>
          Alert.alert("The title", "The message", [
            { text: "Yes", onPress: () => console.log("Yes") },
            { text: "No", onPress: () => console.log("No") },
          ])
        }
      />
      <Button
        color="green"
        title="Sorry, only for iOS"
        onPress={
          (() => Alert.prompt("Prompt title", "Prompt message"),
          (text) => console.log(text))
        }
      />
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

// With StatusBar.currentHeight paddingTop should be just enough to put the first item below the StatusBar but it's a lot more actually
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});
